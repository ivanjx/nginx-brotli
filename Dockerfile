FROM ubuntu

# Install dependencies.
RUN apt-get update && \
    apt-get install -y \
    build-essential \
    libpcre3 \
    libpcre3-dev \
    zlib1g \
    zlib1g-dev \
    openssl \
    libssl-dev \
    wget \
    tar \
    git

ARG NGINX_VERSION=1.21.1
ARG CORE_COUNT=4

# Downloading nginx.
RUN wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
    tar xzf nginx-${NGINX_VERSION}.tar.gz

# Clone brotli.
RUN git clone https://github.com/google/ngx_brotli --recursive

# Build brotli.
RUN cd nginx-${NGINX_VERSION} && \
    ./configure --with-compat --add-dynamic-module=../ngx_brotli && \
    make -j${CORE_COUNT} modules

RUN echo "mkdir -p /modules && cp /nginx-${NGINX_VERSION}/objs/*.so /modules" >> /copy.sh && \
    chmod +x /copy.sh

ENTRYPOINT ["/bin/bash", "/copy.sh"]